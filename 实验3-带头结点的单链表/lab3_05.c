/*
已知线性表存储在带头结点的单链表head中，请设计算法函数void sort(linklist head)，将head中的结点按结点值升序排列。
*/
/**********************************/
/*文件名称：lab3_05.c                 */
/**********************************/
#include "slnklist.h"
/*请将本函数补充完整，并进行测试*/
void  sort(linklist head)
{
linklist pre,p,s,q;
	p=head->next;//初始化 
	head->next=NULL;//初始化，为使q每次从头开始 
	while(p){//大循环 
		s=p;//存储p值 
		p=p->next;//p往下走 
		q=head->next;//从头循环 
		pre=head;
		while(q&&q->info<s->info){//q指向的数判断 
			pre=q;
			q=q->next;	
		}
		pre->next=s;//插入有序列表 
		s->next=q;
	} 
	return q; 
}
int main()
{        linklist head;
         head=creatbyqueue();   		/*尾插法建立带头结点的单链表*/
         print(head);    			    /*输出单链表head*/
         sort(head);     				/*排序*/
         print(head);
         delList(head);
         return 0;
}
