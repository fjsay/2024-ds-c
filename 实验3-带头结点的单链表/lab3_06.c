/*
已知两个带头结点的单链表L1和L2中的结点值均已按升序排序，设计算法函数
linklist mergeAscend (linklist L1,linklist L2)将L1和L2合并成一个升序的
带头结单链表作为函数的返回结果；
设计算法函数linklist mergeDescend (linklist L1,linklist L2)
将L1和L2合并成一个降序的带头结单链表作为函数的返回结果；
并设计main()函数进行测试。
*/
/**********************************/
/*文件名称：lab3_06.c                 */
/**********************************/
#include "slnklist.h"
/*请将本函数补充完整，并进行测试*/
linklist mergeAscend(linklist L1,linklist L2)
{
 linklist L3,r3,p,q;
        L3=r3=(linklist)malloc(sizeof(node));
        p=L1->next;
        q=L2->next;
        while (p && q)
        {
            if (p->info<q->info)
            {
                L1->next=p->next;
                r3->next=p;
                r3=p;
                p=L1->next;
            }
            else
            {
                L2->next=q->next;
                r3->next=q;
                r3=q;
                q=L2->next;
            }
        }
        if (p)  r3->next=p;
        if (q) r3->next=q;
        free(L1);
        free(L2);
        return L3;
}
linklist mergeDescend(linklist L1,linklist L2)
{
	    linklist L3,r3,p,q;
        L3=(linklist)malloc(sizeof(node));
        L3->next=NULL;

        p=L1->next;
        q=L2->next;
        while (p && q)
        {
            if (p->info<q->info)
            {
                L1->next=p->next;
                p->next=L3->next;
                L3->next=p;
                p=L1->next;
            }
            else
            {
                L2->next=q->next;
                q->next=L3->next;
                L3->next=q;
                q=L2->next;
            }
        }
        while (p)
        {
                L1->next=p->next;
                p->next=L3->next;
                L3->next=p;
                p=L1->next;
        }
        while (q)
        {
                L2->next=q->next;
                q->next=L3->next;
                L3->next=q;
                q=L2->next;
        }
        free(L1);
        free(L2);
        return L3;

}
int main()
{       linklist h1,h2,h3;
         h1=creatbyqueue();     /*尾插法建立单链表,请输入升序序列*/
         h2=creatbyqueue();
         print(h1);
         print(h2);
         h3=mergeAscend(h1,h2);/*升序合并到h3*/
            /*降序合并请调用h3=mergeDescend(h1,h2); */
         print(h3);
         delList(h3);
         return 0;
}
