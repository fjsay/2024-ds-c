/*
编写算法函数linklist delallx(linklist head, int x)，删除不带头结点单链表head中所有值为x的结点。
*/
/**********************************/
/*文件名称：lab2_04.c                 */
/**********************************/
#include "slnklist.h"
/*请将本函数补充完整，并进行测试*/
linklist delallx(linklist head,int x)
{
 linklist  pre,p;
    pre=NULL;
    p=head;
    while(p)
    {
              while (p &&p->info!=x)            //找值为x的结点
                {
                        pre=p;
                        p=p->next;
                }
                if (p)                                          //找到了
                {
                     if (pre==NULL)                 //删除的结点为第一个结点
                       {
                            head=p->next;
                            free(p);
                            p=head;
                       }
                    else                                    //删除的结点不是第一个结点
                        {
                            pre->next=p->next;
                            free(p);
                            p=pre->next;
                        }
                }

    }
    return head;
}
int main()
{   datatype x;
    linklist head;
    head=creatbyqueue();				/*尾插入法建立单链表*/
    print(head);
    printf("请输入要删除的值：");
    scanf("%d",&x);
    head=delallx(head,x);
    print(head);
    delList(head);
    return 0;
}
